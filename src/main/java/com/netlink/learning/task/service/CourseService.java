package com.netlink.learning.task.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netlink.learning.task.model.Course;

@Service
public class CourseService {
	@Autowired
	CourseRepository courseRepository;

	public List<Course> getAllCourse() {
		List<Course> courses = new ArrayList<>();
		courseRepository.findAll().forEach(course -> courses.add(course));
		return courses;
	}

	public Course getSubByCode(String subCode) {
		return courseRepository.findById(subCode).get();
	}

	public void delete(String subCode) {

		courseRepository.deleteById(subCode);
	}

	public void update(Course course) {
		courseRepository.save(course);

	}

}
