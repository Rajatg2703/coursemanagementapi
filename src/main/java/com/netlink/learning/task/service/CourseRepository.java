package com.netlink.learning.task.service;

import javax.persistence.Table;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;

import com.netlink.learning.task.model.Course;


@Table(name="course")
public interface CourseRepository extends CrudRepository<Course,String> {

}
