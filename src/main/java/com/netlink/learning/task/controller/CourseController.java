package com.netlink.learning.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.netlink.learning.task.model.Course;
import com.netlink.learning.task.service.CourseService;

@RestController
public class CourseController {

	
	@Autowired
	CourseService courseService;

	@GetMapping("/course")
	private List<Course> getAllCourse() {
		return courseService.getAllCourse();
	}

	@GetMapping("/student/{subcode}")
	private Course getCourse(@PathVariable("subcode") String subCode) {
		return courseService.getSubByCode(subCode);
	}

	@DeleteMapping("/course/{subcode}")
	private void deleteStudent(@PathVariable("subcode") String subCode) {
		courseService.delete(subCode);
	}

	@PostMapping("/course")
	private void saveStudent(@RequestBody Course course) {
		courseService.update(course);
	}
}
