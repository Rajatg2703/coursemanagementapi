package com.netlink.learning.task.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="course")
public class Course {
	
	@Id
	private int id;  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	 
	private CourseUnit [] syllabus;
	
	private String subCode;
	private String name;
	private String description;
	private String prerequisite;
	
	public Course() {
		
	}
	public Course(int id,String subCode, String name, String description, String prerequisite) {
		super();
		this.id=id;
		this.subCode = subCode;
		this.name = name;
		this.description = description;
		this.prerequisite = prerequisite;
	}
	
	public String getSubCode() {
		return subCode;
	}
	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPrerequisite() {
		return prerequisite;
	}
	public void setPrerequisite(String prerequisite) {
		this.prerequisite = prerequisite;
	}
	
	
	
}
