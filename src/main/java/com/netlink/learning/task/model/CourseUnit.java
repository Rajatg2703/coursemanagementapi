package com.netlink.learning.task.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="syllabus")
@Getter // Lombok
@Setter // Lombok
@NoArgsConstructor // Lombok
public class CourseUnit {
	private String id;
	private String subjectCode;
	private int unitNumber;
	private String name;
	private String contents;
}
